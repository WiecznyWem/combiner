# Combiner
An application that helps you to learn any language by preparing audio/video files with speech that is repeated. Between every repetition there is a moment of silence for you to try to say the sentence on your own.
It can be used in a car or in home. 

## Usage case 1
You have a movie with subtitles that you watch using VLC. You can mark interesting moments using `lurker` extension. It produces a textfile in the directory of your movie `lurker.txt`. You use `cutter` to first extract those moments, and then `combiner` to compile them into repeated form. 

## Usage case 2:
You cannot access your movie with VLC, so you use NVIDIA Shadowplay to record your screen. Then you use the resulting files in `combiner`. 



## Requires:
* Python 3
* **ffmpeg 4.2**+, accessible form PATH
* `pip install Gooey`
* `pip install pysubs2`

## Notes:
Put `lurker.luac` from `vlc_extension` into `VLC\lua\extensions`.

VLC shortcuts can be accessed by pressing ALT. 

Use JoyToKey to bind Shadowplay/vlc shortcuts to your XBOX pad, so you can mark moments while lying on a bed. 

It is good to keybind Shadowplay's `Start/Stop recording` to `Scroll lock`, so you can see on your keyboard when it records. 

To run without GUI use parameter `--ignore-gooey`

Windows shortcut example:
"C:\Program Files\Python37\pythonw.exe" [DIR]/app/main.py