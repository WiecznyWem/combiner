import subprocess

def shellExecute(command, deadly = True):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    if process.returncode != 0:
        print("An error occurred :", process.returncode)
        print("COMMAND: ", command)
        print("OUT: ", out)
        print("ERR: ", err)
        if deadly:
            exit(1)
    return out, err

def legalizeFilename(filename: str):
    illegal = ['NUL','\\N', '\',''//',':','*','"','<','>','|', '?']
    for i in illegal:
        filename = filename.replace(i, '')
    return filename