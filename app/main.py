from cutter import Cutter
from combiner import Combiner
from audioExtractor import AudioExtractor
import gooey 

@gooey.Gooey(program_name='Eng Combiner', tabbed_groups=True, progress_regex=r"^Processing file  (?P<current>\d+) \/ (?P<total>\d+)", progress_expr="(current - 1) / total * 100")
def main():
    parser = gooey.GooeyParser(description='Eng Video compilator')
    subs = parser.add_subparsers(dest='mainCommand')

    cutter_parser = subs.add_parser('cutter')

    required = cutter_parser.add_argument_group(
        'Required',
        gooey_options={
            'columns': 1,
        }
    )

    required.add_argument('lurkerFile', 
                metavar='Lurker output', 
                help='Select lurker.txt file', 
                widget='FileChooser',
                type=str)

    required.add_argument('videoFile', 
                metavar='Video File', 
                help='Select video file', 
                widget='FileChooser',
                type=str)

    required.add_argument('destinationDir', 
                metavar='Destination', 
                help='Select destination directory', 
                widget='DirChooser',
                type=str)

    optionals = cutter_parser.add_argument_group(
        'Optional',
        gooey_options={
            'columns': 2,
        }
    )

    optionals.add_argument('--subtitlesFile', metavar='Subtitles', help='Select subtitles file', required=False, widget='FileChooser', default="", type=str)
    optionals.add_argument('--videoOutput', metavar="Video output", help='Produce video output', required=False, widget="CheckBox", action='store_true')
    optionals.add_argument('--videoQuality', metavar="Video quality", help="H.264's CRF value", required=False, default="23", type=str)
    optionals.add_argument('--audioQuality', metavar="Audio quality", help="LAME's VBR value", required=False, default="2", type=str)

    combiner_parser = subs.add_parser('combiner')

    required = combiner_parser.add_argument_group(
        'Required',
        gooey_options={
            'columns': 1,
        }
    )

    required.add_argument('source', 
                metavar='Source', 
                help='Source folder with video files', 
                widget='DirChooser',
                type=str)
    required.add_argument('destination', 
                metavar='Destination', 
                help='Destination folder where complied videos will be stored', 
                widget='DirChooser',
                type=str)

    combinerArguments = combiner_parser.add_argument_group(
        'Combiner',
        gooey_options={
            'columns': 2,
        }
    )

    combinerArguments.add_argument('--overwrite', metavar='Overwrite', help='the destination folder will be overwritten', required=False, widget="CheckBox", action='store_true')
    combinerArguments.add_argument('--repeats',  metavar="Number of repeats", help='Number of repeats of each video in compiled video', required=False, default=5, type=int)
    combinerArguments.add_argument('--silence_padding', metavar="Silence padding", help='Length of silence inserted after every repetition.', required=False, default=2.5, type=float)
    
    silenceArguments = combiner_parser.add_argument_group(
        'Silence',
        gooey_options={
            'columns': 1,
        }
    )
    silenceArguments.add_argument('--allow_silence_cutting',  metavar='Allow silence cutting',  help='If selected, silences on begining and ending will be cutted off',   required=False,  widget="CheckBox", action='store_true')
    silenceArguments.add_argument('--allow_silence_middle_cut',  metavar='Middle cut',  help='If selected, silences in the middle will be cutted off',   required=False,  widget="CheckBox", action='store_true')
    silenceArguments.add_argument('--silence_threshold', metavar="Silence Threshold", help='Threshold used to determine silence analysing s16le data', required=False, default=100, type=int)
    silenceArguments.add_argument('--audio_sampling', metavar="Audio Frequency sampling", help='Audio sampling used to determine silence analysing s16le data', required=False, default=16000, type=int)
    silenceArguments.add_argument('--shortest_silence', metavar="Shortest silence window", help='Shortest possible duration of silence in seconds', required=False, default=0.05, type=float)
    
    normalizationArguments = combiner_parser.add_argument_group(
        'Normalization',
        gooey_options={
            'columns': 1,
        }
    )
    normalizationArguments.add_argument('--allow_normalization',  metavar='Allow normalization',  help='If selected audio will be normalized',   required=False,  widget="CheckBox", action='store_true')
    normalizationArguments.add_argument('--target_I', metavar="Integrated loudness target", help='Normalization integrated loudness target parameter for first pass of ffmpeg\' loudnorm. Range [-70.0 - -5.0]', required=False, default=-24, type=float)
    normalizationArguments.add_argument('--target_LRA', metavar="Loudness range target", help='Normalization loudness range target parameter for first pass of ffmpeg\' loudnorm. Range [1.0 - 20.0]', required=False, default=7, type=float)
    normalizationArguments.add_argument('--target_TP', metavar="Maximum true peak", help='Normalization maximum true peak parameter for first pass of ffmpeg\' loudnorm. Range [-9.0 - 0.0]', required=False, default=-2, type=float)
    
    ffmpegArguments = combiner_parser.add_argument_group(
        'ffmpeg',
        gooey_options={
            'columns': 1,
        }
    )
    ffmpegArguments.add_argument('--videoQuality', metavar="Video quality", help="H.264's CRF value", required=False, default="23", type=str)
    ffmpegArguments.add_argument('--audioQuality', metavar="Audio quality", help="LAME's VBR value", required=False, default="2", type=str)


    audioExtractor_parser = subs.add_parser('audioExtractor')

    requiredAE = audioExtractor_parser.add_argument_group(
        'Required',
        gooey_options={
            'columns': 1,
        }
    )

    requiredAE.add_argument('source', 
                metavar='Source', 
                help='Source folder with video files', 
                widget='DirChooser',
                type=str)
    requiredAE.add_argument('destination', 
                metavar='Destination', 
                help='Destination folder where complied audio files will be stored', 
                widget='DirChooser',
                type=str)

    args = parser.parse_args()     

    if args.mainCommand == "cutter":
        cutter = Cutter(**vars(args))
        cutter.process()              

    if args.mainCommand == "combiner":
        combiner = Combiner(**vars(args))
        combiner.process()              

    if args.mainCommand == "audioExtractor":
        extractor = AudioExtractor(**vars(args))
        extractor.process()              

if __name__ == '__main__':
    main()