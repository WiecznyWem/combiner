import glob
import os 
import subprocess

class AudioExtractor:
    def __init__(self, source, destination, overwrite = False, mainCommand=None):
        self.source = source
        self.destination = destination
        self.overwrite = overwrite

    def process(self):
        self.__prepare_workspace()
        self.__validate_parameters()
        self.__processFiles(); 
    
    def __validate_parameters(self):
        dest = os.listdir(self.destination)
        if self.overwrite == False and len(dest) != 0:
            print("Destination directory is not empty. ")
            exit(1)

        if self.source == self.destination:
            print("Destination directory matches the Source directory. ")
            exit(1)

    def __prepare_workspace(self):
        if not os.path.exists(self.destination):
            os.makedirs(self.destination)

    def __processFiles(self):
        files = [os.path.basename(x) for x in glob.glob(self.source+'\\*.mp4')]
        totalFiles = len(files)
        for index, file in enumerate(files, 1):
            print("Processing file ", index, "/", totalFiles, ": ", file)
            baseFileName = os.path.splitext(file)[0]
            self.__processFile(baseFileName)

    def __processFile(self, filename):
        input_path = self.source + "\\" + filename + ".mp4"
        output_path  = self.destination + "\\" + filename + ".mp3"
        self.__execute(["ffmpeg", "-y", "-i", input_path, "-acodec", "libmp3lame", "-ac", "2", "-q:a", "0", output_path])


    def __execute(self, command, deadly = True):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        if process.returncode != 0:
            print("An error occurred :", process.returncode)
            print("COMMAND: ", command)
            print("OUT: ", out)
            print("ERR: ", err)
            if deadly:
                exit(1)
        return out, err
