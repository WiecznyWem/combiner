import os
import glob
import argparse
import subprocess
import struct 
import shutil
import json
import collections 

class Combiner:
    def __init__(self, source: str, destination: str,
        overwrite: bool=False, allow_silence_middle_cut: bool=False, repeats: int=5, silence_padding: float=2, silence_threshold: int=1000, audio_sampling: int=16000, 
        shortest_silence: float=0.05, target_I: float=-24, target_LRA: float=7, target_TP: float=-2, videoQuality: str="23", audioQuality: str="2", allow_silence_cutting: bool=False, allow_normalization: bool=False, mainCommand=None):

        self.source = source
        self.destination = destination

        self.overwrite = overwrite
        self.allow_silence_cutting = allow_silence_cutting
        self.allow_silence_middle_cut = allow_silence_middle_cut
        self.repeats = repeats
        self.silence_padding = silence_padding

        self.allow_normalization = allow_normalization
        self.silence_threshold = silence_threshold
        self.audio_sampling = audio_sampling
        self.shortest_silence = shortest_silence
        self.target_I = target_I
        self.target_LRA = target_LRA
        self.target_TP = target_TP


        self.audioQuality = audioQuality
        self.videoQuality = videoQuality
        self.__working_directory=self.destination+"\\tmp\\"

    def process(self):
        self.__prepare_workspace()
        self.__validate_parameters()
        self.__processFiles()
        self.__cleanup_workspace()


    def __validate_parameters(self):
        dest = os.listdir(self.destination)
        if self.overwrite == False and len(dest) != 1:
            print("Destination directory is not empty. ")
            exit(1)

        if self.source == self.destination:
            print("Destination directory matches the Source directory. ")
            exit(1)
    

    def __prepare_workspace(self):
        if not os.path.exists(self.destination):
            os.makedirs(self.destination)
        
        if not os.path.exists(self.__working_directory):
            os.makedirs(self.__working_directory)


    def __processFiles(self):
        files = [os.path.basename(x) for x in glob.glob(self.source+'\\*.mp*') if x[-1]=='4' or x[-1]=='3']
        totalFiles = len(files)
        for index, file in enumerate(files, 1):
            print("Processing file ", index, "/", totalFiles, ": ", file)
            self.__processFile(file)


    def __cleanup_workspace(self):
        shutil.rmtree(self.__working_directory)

    def __simplifyName(self, name):
        return name.replace('\'', '')

    def __processFile(self, file):
        steps = 3 
        if not self.allow_normalization:
            steps -= 1

        file_paths = []
        file_paths.append(self.source  + "\\" + file)
        for x in range(1, steps + 1): 
            file_paths.append(self.__working_directory +  "\\" + self.__simplifyName(file) + "_step" + str(x) + ".mp" + file[-1])
        file_paths.append(self.destination + "\\" + file )

        step = 1

        if self.allow_silence_cutting:
            self.__cut_off_silences(file_paths[step-1], file_paths[step])
        else:
            self.__recode(file_paths[step-1], file_paths[step])
        step += 1

        self.__appendSilence(file_paths[step-1], file_paths[step])
        step += 1

        self.__repeat(file_paths[step-1], file_paths[step])
        step += 1
        
        if self.allow_normalization:
            self.__normalize(file_paths[step-1], file_paths[step])
            step += 1

    def __recode(self, input, output):
            self.__execute(["ffmpeg", "-y",  "-i", input, "-c:v", "libx264", "-crf",  self.videoQuality, "-c:a", "libmp3lame", "-q:a", self.audioQuality, output])
    
    def __normalize(self, input, output):
        out, err = self.__execute(["ffmpeg", "-i", input, "-af", "loudnorm=I="+str(self.target_I)+":dual_mono=true:TP="+str(self.target_TP)+":LRA="+str(self.target_LRA)+":print_format=json", "-f", "null", "-"])
        json_code = err[err.find(b"{\r\n\t\"input_i\""):]
        jd = json.loads(json_code)

        self.__execute(["ffmpeg", "-y", "-i", input, "-vcodec", "copy", "-af", "loudnorm=I="+str(self.target_I)+":dual_mono=true:TP="+str(self.target_TP)+":LRA="+str(self.target_LRA)+":measured_I="+jd["input_i"]+":measured_LRA="+jd["input_lra"]+":measured_TP="+jd["input_tp"]+":measured_thresh="+jd["input_thresh"]+":offset="+jd["target_offset"]+":linear=true", "-fflags", "+genpts",  "-stats", output])


    def __appendSilence(self, input, output):
        self.__execute(["ffmpeg", "-y", "-i", input, "-vf", "tpad=stop_mode=clone:stop_duration="+str(self.silence_padding), "-shortest", "-af", "apad=pad_dur="+str(self.silence_padding), output])

    def __repeat(self, input, output):
        summary_file_path   = self.__working_directory  + "\\" + self.__simplifyName(os.path.basename(input)) + "_summary.txt"
        with open(summary_file_path, "w") as summaryFile:
            for _ in range(self.repeats):
                summaryFile.write("file '" + input + "'\n")
            summaryFile.close()
        self.__execute(["ffmpeg", "-y", "-f", "concat", "-safe", "0", "-i", summary_file_path, "-c", "copy",  "-fflags", "+genpts", "-stats", output])


    def __cut_off_silences(self, input, output):
        pcm_file_path  = self.__working_directory + "\\" + self.__simplifyName(os.path.basename(input)) + "_tmp.pcm"

        self.__extract_pcm(input, pcm_file_path)
        silences, size = self.__detect_silences(pcm_file_path)
        ranges = self.__silences_complement(silences, size)

        if self.allow_silence_middle_cut == False:
            ranges = [[ranges[0][0], ranges[-1][1]]]
        
        interval_file_paths = self.__extract_intervals(input, ranges)
        self.__combineFiles(interval_file_paths, output)


    def __combineFiles(self, inputs, output):
        summary_file_path   = self.__working_directory  + "\\" + self.__simplifyName(os.path.basename(output)) + "_summary.txt"
        with open(summary_file_path, "w") as summaryFile:
            for path in inputs:
                summaryFile.write("file '" + path + "'\n")
            summaryFile.close()
        self.__execute(["ffmpeg", "-y", "-f", "concat", "-safe", "0", "-i", summary_file_path, "-c", "copy",  "-fflags", "+genpts", "-stats", output])


    def __extract_intervals(self, input, ranges):
        interval_file_paths = [(self.__working_directory + "\\" + self.__simplifyName(os.path.basename(input)) + "_tmp_" + str(index) + ".mp4") for index in range(len(ranges))]

        for index, (interval, interval_file) in enumerate(zip(ranges, interval_file_paths)):
            start = interval[0]
            time = interval[1] - interval[0]
            self.__execute(["ffmpeg", "-y",  "-ss",str(start), "-i", input, "-t", str(time), "-c:v", "libx264", "-crf",  self.videoQuality, "-c:a", "libmp3lame", "-q:a", self.audioQuality, 
                           "-fflags", "+genpts",  "-stats", interval_file])

        return interval_file_paths


    def __silences_complement(self, silences, pcm_size):
        ranges = []

        if len(silences) == 0:
            ranges.append([0, pcm_size / self.audio_sampling])
        
        if len(silences) > 0 and silences[0][0] != 0:
            ranges.append([0, silences[0][0] / self.audio_sampling ])

        for it in range(len(silences)-1):
            ranges.append([silences[it][1] / self.audio_sampling, silences[it+1][0] / self.audio_sampling])

        if len(silences) > 0 and silences[len(silences)-1][1] != pcm_size:
            ranges.append([silences[len(silences)-1][1] / self.audio_sampling, pcm_size / self.audio_sampling])

        return ranges


    def __detect_silences(self, pcm_input):
        silences = []
        silenceStart = -1

        counter = 0
        with open(pcm_input, "rb") as file:
            while True:
                chunk = file.read(2)
                if chunk:
                    value = struct.unpack("<h", chunk)[0]
                    if abs(value) < self.silence_threshold:
                        if silenceStart == -1:
                            silenceStart = counter
                    else:
                        if silenceStart != -1:
                            if (counter - silenceStart) / self.audio_sampling > self.shortest_silence:
                                silences.append([silenceStart, counter])
                        silenceStart = -1
                else:
                    break
                counter += 1

        if silenceStart != -1:
            if (counter - silenceStart) / self.audio_sampling > self.shortest_silence:
                silences.append([silenceStart, counter])
        return silences, counter


    def __extract_pcm(self, input, output):
        self.__execute(["ffmpeg", "-y", "-i", input, "-map", "0:a:0", "-acodec", "pcm_s16le", "-f", "s16le", "-ac", "1", "-ar", str(self.audio_sampling), "-stats", output])


    def __execute(self, command, deadly = True):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        if process.returncode != 0:
            print("An error occurred :", process.returncode)
            print("COMMAND: ", command)
            print("OUT: ", out)
            print("ERR: ", err)
            if deadly:
                exit(1)
        return out, err