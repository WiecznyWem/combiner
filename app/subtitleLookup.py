import pysubs2
import bisect

class SubtitleLookup:
    def __init__(self, subtitleFile: str):
        subs = pysubs2.load(subtitleFile)
        self.__total = len(subs)
        self.__points = [ point  for x in subs for point in [x.start / 1000.0, x.end/1000.0]]
        sorted = all(self.__points[i] <= self.__points[i+1] for i in range(self.__total-1))
        if not sorted:
            print("The subtitles are not muttually exclusive. Potential issues")
        self.__starts = [x.start / 1000.0 for x in subs]
        self.__ends = [x.end / 1000.0 for x in subs]
        self.__texts = [x.text for x in subs]

    def lookupRange(self, queryFrom, queryTo):
        
        indexFrom = bisect.bisect_left(self.__points, queryFrom)- 1
        indexTo = bisect.bisect_left(self.__points, queryTo) -1 

        if indexFrom == indexTo:
            if indexTo % 2 == 1:
                return ""
            else:
                return [self.__texts[indexFrom//2]]

        if indexFrom % 2 == 1:
            indexFrom += 1

        return self.__texts[indexFrom//2:indexTo//2+1]
        