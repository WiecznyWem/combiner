from tools import shellExecute, legalizeFilename
from subtitleLookup import SubtitleLookup
import math

class Cutter:
    def __init__(self, lurkerFile: str, videoFile: str, destinationDir: str,
        subtitlesFile:str = "", videoOutput: bool = False, audioQuality: str = "2", videoQuality: str = "23",
        mainCommand=None):
        self.__ranges = []

        self.lurkerFile = lurkerFile 
        self.videoFile = videoFile 
        self.destinationDir = destinationDir
        self.videoOutput = videoOutput 

        self.audioQuality = audioQuality
        self.videoQuality = videoQuality

        self.__subtitles = None
        self.__decimals = 0
        if subtitlesFile != "":
            self.__subtitles = SubtitleLookup(subtitlesFile)

    def process(self):
        self.__readLurkerFile()
        self.__cutRanges()

    def __readLurkerFile(self):
        with open(self.lurkerFile, "r") as file:
            lines = file.readlines()
            for line in lines:
                if line.find("pop") != -1:
                    self.__ranges.pop();
                comma = line.find(',')
                if comma != -1:
                    a = int(line[:comma]) / 1000000.0
                    b = int(line[comma+1:]) / 1000000.0
                    self.__ranges.append([a,b])
            file.close()
        self.__decimals = math.floor(1 + math.log10(len(self.__ranges)))
        

    def __determineFilename(self, i, range):
        filename = self.destinationDir + "/" + str(i).zfill(self.__decimals) 

        if self.__subtitles != None:
            subs = self.__subtitles.lookupRange(range[0], range[1])
            if subs != None:
                desc = ''.join(subs)
                filename += " " + legalizeFilename(desc)
        if self.videoOutput:
            filename += ".mp4"
        else:
            filename += ".mp3"

        return filename

    def __cutRanges(self):
        total = len(self.__ranges)
        for (i, range) in enumerate(self.__ranges):
            i += 1;
            outputFile = self.__determineFilename(i, range)

            print("Processing file ", i, "/", total, ": ", outputFile)

            if self.videoOutput:
                self.__splitVideo(range, outputFile)
            else:
                self.__splitAudio(range, outputFile)

    def __splitVideo(self, range, outputFile):
        command = ["ffmpeg", "-y",  "-ss",str(range[0]), "-i", self.videoFile, "-t", str(range[1] - range[0])]
        command += ["-c:v", "libx264", "-crf",  self.videoQuality, "-c:a", "libmp3lame", "-q:a", self.audioQuality]
        command.append(outputFile)
        shellExecute(command)
    
    def __splitAudio(self, range, outputFile):
        command = ["ffmpeg", "-y", "-ss",str(range[0]), "-i", self.videoFile, "-t", str(range[1] - range[0])]
        command += ["-vn", "-c:v", "libmp3lame", "-q:a", self.audioQuality]
        command.append(outputFile)
        shellExecute(command)